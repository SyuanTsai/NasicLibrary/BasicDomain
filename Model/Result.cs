﻿namespace BasicDomain.Model
{
    /// <summary>
    ///     ##### 共用Model
    ///     返回結果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T>
    {
        /// <summary>
        ///     本次請求最後是否成功。
        /// </summary>
        public bool Success { get; set; } = false;

        /// <summary>
        ///     回應代碼
        /// </summary>
        /// <remarks>
        ///     這個回應代碼是錯誤的Error Code。
        /// </remarks>
        public int Code { get; set; } = 0;

        /// <summary>
        ///     對外回應的結果描述(文字版)
        /// </summary>
        /// <remarks>
        ///     此內容需要可以讓使用者知道目前回應的狀態結果為何，
        ///     不能是模稜良可的回應訊息。
        /// </remarks>
        /// <example>
        ///     舉例：
        /// <code>
        ///     //  錯誤的訊息內容，無法讓使用者知道在哪邊失敗。
        ///     string MessageFail = "失敗";
        /// 
        ///     //  正確的訊息內容，能夠讓使用者知道目前回應的狀態，也不會將系統內的錯誤訊息給釋放出去。
        ///     string MessageSuccess = "遠端資料庫查無資料。";
        /// </code>
        /// </example>
        public string Message { get; set; } = "Msg Undefined Place Connect Maintainer To Fix It.";

        /// <summary>
        ///     真實錯誤的詳細資訊，
        ///     需要在程式中做處理，根據權限決定是否給出錯誤訊息。
        /// </summary>
        //  Todo 實作相關功能
        public string DetailMessage { get; set; } = "";


        /// <summary>
        ///     回應的實際資料內容。
        /// </summary>
        public T Data { get; set; }
    }
}